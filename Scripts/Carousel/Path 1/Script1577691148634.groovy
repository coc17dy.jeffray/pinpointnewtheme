import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://ezcheck-in-nexttheme-dev.e4score.com/')

WebUI.maximizeWindow()

WebUI.verifyElementVisible(findTestObject('Carousel/Element Verification/(v)'))

WebUI.verifyElementVisible(findTestObject('Carousel/Element Verification/(v)2'))

WebUI.verifyElementVisible(findTestObject('Carousel/Element Verification/button_Register Now'))

WebUI.verifyElementVisible(findTestObject('Carousel/Element Verification/h3_EZCheck-In'))

WebUI.verifyElementVisible(findTestObject('Carousel/Element Verification/img_Not yet registered_truck-logo'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Previous_carousel-control-next-icon'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Previous_carousel-control-next-icon'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Previous_carousel-control-next-icon'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Previous_carousel-control-next-icon'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Previous_carousel-control-next-icon'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Out-perform your competitors Grow now__85d6a9'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Out-perform your competitors Grow now__85d6a9'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Out-perform your competitors Grow now__85d6a9'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Out-perform your competitors Grow now__85d6a9'))

WebUI.click(findTestObject('Object Repository/Carousel/span_Out-perform your competitors Grow now__85d6a9'))

WebUI.click(findTestObject('Object Repository/Carousel/i_Next_fa fa-pause'))

WebUI.click(findTestObject('Object Repository/Carousel/i_Next_fa fa-play'))

WebUI.click(findTestObject('Object Repository/Carousel/li'))

WebUI.click(findTestObject('Object Repository/Carousel/li_1'))

WebUI.delay(3)

WebUI.closeBrowser()

