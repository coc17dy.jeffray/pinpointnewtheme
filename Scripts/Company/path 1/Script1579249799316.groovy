import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://ezcheck-in-nexttheme-dev.e4score.com/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Company/input_Login_form-control'), 'maintester12@gmail.com')

WebUI.setEncryptedText(findTestObject('Company/input_Login_form-control_1'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.click(findTestObject('Company/button_Login'))

WebUI.click(findTestObject('Company/a_Company'))

WebUI.click(findTestObject('Object Repository/Company/input_Legal Entity Name _name'))

WebUI.setText(findTestObject('Company/input_Legal Entity Name _name'), 'Test Pinpoint')

WebUI.clearText(findTestObject('Company/input_DBA Name_dbaName'))

WebUI.click(findTestObject('Company/div_DBA Name_input-group'))

WebUI.setText(findTestObject('Company/input_DBA Name_dbaName'), 'Pinpoint Corp DBA')

WebUI.clearText(findTestObject('Company/input_Address 1 _address1'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Company/input_Address 1 _address1'))

WebUI.setText(findTestObject('Company/input_Address 1 _address1'), '750 Kearny St, San Francisco, CA 94108, United States')

WebUI.clearText(findTestObject('Company/input_Address 2_address2'))

WebUI.click(findTestObject('Company/div_DBA Name_input-group'))

WebUI.setText(findTestObject('Company/input_Address 2_address2'), '754 Kearny St, San Francisco, CA 94108, United States')

WebUI.click(findTestObject('Company/div_United States'))

WebUI.click(findTestObject('Company/div_United States_1'))

WebUI.clearText(findTestObject('Company/div_DBA Name_input-group'))

WebUI.setText(findTestObject('Company/input_City _city'), 'San Francisco')

WebUI.click(findTestObject('Company/div_California_1'))

WebUI.click(findTestObject('Object Repository/Company/div_California_1'))

WebUI.clearText(findTestObject('Company/input_Zip Code _postalCode'))

WebUI.setText(findTestObject('Company/input_Zip Code _postalCode'), '94108')

WebUI.click(findTestObject('Company/div_Miles'))

WebUI.click(findTestObject('Company/div_Miles_1'))

WebUI.click(findTestObject('Company/button_Save'))

WebUI.delay(3)

WebUI.closeBrowser()

