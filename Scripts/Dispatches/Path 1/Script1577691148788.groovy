import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://ezcheck-in-nexttheme-dev.e4score.com/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Dispatch/input_Login_form-control'), 'maintester12@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Dispatch/input_Login_form-control_1'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.click(findTestObject('Object Repository/Dispatch/button_Login'))

WebUI.click(findTestObject('Object Repository/Dispatch/a_Dispatches'))

WebUI.click(findTestObject('Object Repository/Dispatch/button_New Dispatch'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_Manually_css-1uq0kb5 select__indicator _8bb7bc'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_At DateTime'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_TSP 1 _css-1hwfws3 select__value-contai_7172ea'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_Pinpoint Carrier 1'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_TSP 1 _css-1hwfws3 select__value-contai_7172ea'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_Empty_css-1uq0kb5 select__indicator sel_b5bdfe'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_Empty'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_Shipment ID 1_css-1uq0kb5'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_PO 0061'))

WebUI.click(findTestObject('Object Repository/Dispatch/div_Start Typing to Look Up Reference Numbe_09a9cc'))

WebUI.setText(findTestObject('Dispatch/DispatchRefNum'), '123456')

WebUI.sendKeys(findTestObject('Object Repository/Dispatch/input_Start Typing to Look Up Reference Num_27335f'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Dispatch/div_Start typing to lookup tractor_css-1uq0_a38c6b'))

WebUI.setText(findTestObject('Object Repository/Dispatch/input_Start typing to lookup tractor_react-_bf9402'), '1')

WebUI.sendKeys(findTestObject('Object Repository/Dispatch/input_Start typing to lookup tractor_react-_bf9402'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Dispatch/div_Start typing to lookup tractor_css-1uq0_a38c6b'))

WebUI.setText(findTestObject('Object Repository/Dispatch/input_Start typing to lookup trailer_react-_d690c1'), '2')

WebUI.sendKeys(findTestObject('Object Repository/Dispatch/input_Start typing to lookup trailer_react-_d690c1'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Dispatch/button_Save Dispatch'))

WebUI.closeBrowser()

