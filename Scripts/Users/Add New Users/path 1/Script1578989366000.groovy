import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://ezcheck-in-nexttheme-dev.e4score.com/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Users/input_Login_form-control'), 'maintester12@gmail.com')

WebUI.setEncryptedText(findTestObject('Users/input_Login_form-control_1'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.click(findTestObject('Users/button_Login'))

WebUI.click(findTestObject('Users/a_Users'))

WebUI.click(findTestObject('Users/button_Add New User(Exact)'))

WebUI.setText(findTestObject('Users/input_Email _email'), 'maintester15@gmail.com')

WebUI.setText(findTestObject('Users/input_First Name _firstname'), 'Train')

WebUI.setText(findTestObject('Users/input_Last Name _lastname'), 'Heartnet')

WebUI.click(findTestObject('Users/div_Email First Name Last Name Phone 1Mobil_3dd7e5'))

WebUI.setText(findTestObject('Users/input_Phone_phone'), '232-141-2412')

WebUI.click(findTestObject('Users/div_1'))

WebUI.setText(findTestObject('Users/input_Mobile_cell'), '235-321-4212')

WebUI.click(findTestObject('Users/div_Enabled_react-switch-bg'))

WebUI.click(findTestObject('Users/div_SelectLocation(Exact)'))

WebUI.click(findTestObject('Users/div_Area 51'))

WebUI.delay(2)

WebUI.click(findTestObject('Users/div_SelectRoles(Exact)'))

WebUI.click(findTestObject('Users/div_Accounts Payable(Exact)'), FailureHandling.STOP_ON_FAILURE)

WebUI.setEncryptedText(findTestObject('Users/input_Password _password'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.setEncryptedText(findTestObject('Users/input_Password confirm _passwordConfirm'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.delay(3)

WebUI.click(findTestObject('Users/button_Save'))

WebUI.delay(2)

WebUI.closeBrowser()

