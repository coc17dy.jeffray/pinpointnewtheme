import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://ezcheck-in-nexttheme-dev.e4score.com/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Carriers/input_Login_form-control'), 'maintester12@gmail.com')

WebUI.setEncryptedText(findTestObject('Carriers/input_Login_form-control_1'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.click(findTestObject('Carriers/button_Login'))

WebUI.verifyElementVisible(findTestObject('Carriers/button_Add New Carrier 2'))

WebUI.click(findTestObject('Carriers/button_Add New Carrier 2'))

WebUI.verifyElementVisible(findTestObject('Carriers/input_Name _name'))

WebUI.verifyElementVisible(findTestObject('Carriers/input_National Registration Number _nationa_7a7a63'))

WebUI.verifyElementVisible(findTestObject('Carriers/input_Enter one or more Short Codes_react-s_beb17a'))

WebUI.click(findTestObject('Carriers/button_SaveCarrier(Exact)'))

WebUI.click(findTestObject('Carriers/Add Button 3'))

WebUI.verifyElementVisible(findTestObject('Carriers/input_First Name _firstName'))

WebUI.verifyElementVisible(findTestObject('Carriers/input_Last Name _lastName'))

WebUI.verifyElementVisible(findTestObject('Carriers/input_Email _email'))

WebUI.verifyElementVisible(findTestObject('Carriers/input_Phone_phone'))

WebUI.verifyElementVisible(findTestObject('Carriers/input_Mobile_cell'))

WebUI.click(findTestObject('Carriers/button_SaveADD(exact)'))

WebUI.delay(2)

WebUI.closeBrowser()

