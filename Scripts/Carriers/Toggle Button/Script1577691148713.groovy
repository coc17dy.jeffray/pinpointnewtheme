import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://ezcheck-in-nexttheme-dev.e4score.com/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Carriers/input_Login_form-control'), 'maintester12@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Carriers/input_Login_form-control_1'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.click(findTestObject('Object Repository/Carriers/button_Login'))

WebUI.click(findTestObject('Object Repository/Carriers/a_Carriers'))

WebUI.click(findTestObject('Object Repository/Carriers/div_Download Import Template_react-switch-bg'))

WebUI.verifyElementVisible(findTestObject('Carriers/toggle input field'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Carriers/div_Download Import Template_react-switch-handle'))

WebUI.verifyElementNotVisible(findTestObject('Carriers/toggle input field'))

WebUI.delay(3)

WebUI.closeBrowser()

