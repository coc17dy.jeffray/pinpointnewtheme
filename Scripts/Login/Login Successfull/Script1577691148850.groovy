import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://ezcheck-in-nexttheme-dev.e4score.com/')

WebUI.maximizeWindow()

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/button_Forgot password'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/button_Login'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/img_Login_login-logo'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/input_Login_form-control'))

WebUI.setText(findTestObject('Login/input_Login_form-control'), 'maintester12@gmail.com')

WebUI.setEncryptedText(findTestObject('Login/input_Login_form-control_1'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/a_Administration(2)'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/a_Dispatch  Assessorials(2)'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/a_Dispatch Assets(2)'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/a_Dispatches(2)'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/a_Shipments(2)'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/Hamburger(2)'))

WebUI.verifyElementVisible(findTestObject('Login/Element Verification/Header(2)'))

WebUI.delay(3)

WebUI.closeBrowser()

