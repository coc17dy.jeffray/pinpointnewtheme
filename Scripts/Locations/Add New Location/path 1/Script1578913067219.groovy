import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://ezcheck-in-nexttheme-dev.e4score.com/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Locations/input_Login_form-control'), 'maintester12@gmail.com')

WebUI.setEncryptedText(findTestObject('Locations/input_Login_form-control_1'), 'SYb/3ow/20+o2oI9OaGCEA==')

WebUI.click(findTestObject('Locations/button_Login'))

WebUI.click(findTestObject('Locations/a_Locations'))

WebUI.click(findTestObject('Locations/button_Add New Location'))

WebUI.setText(findTestObject('Locations/input_Name _name'), 'Area 51')

WebUI.setText(findTestObject('Locations/input_Address 1 _address1'), '750 Kearny St, San Francisco, CA 94108, United States')

WebUI.setText(findTestObject('Locations/input_Address 2_address2'), '754 Kearny St, San Francisco, CA 94108, United States')

WebUI.setText(findTestObject('Locations/input_City _city'), 'San Francisco')

WebUI.click(findTestObject('Locations/div_Select'))

WebUI.setText(findTestObject('Locations/input_Select_react-select-8-input'), 'c')

WebUI.click(findTestObject('Locations/div_California'))

WebUI.setText(findTestObject('Locations/input_Contact Email Address _email'), 'maintester8@gmail.com')

WebUI.setText(findTestObject('Locations/input_Contact First Name _contactFirstName'), 'Creed')

WebUI.setText(findTestObject('Locations/input_Contact Last Name _contactLastName'), 'Diskence')

WebUI.setText(findTestObject('Locations/input_Location Code_code'), '321')

WebUI.setText(findTestObject('Locations/input_Location Nickname_nickname'), 'area15')

WebUI.click(findTestObject('Locations/div_EZCheck-In Site_react-switch-bg'))

WebUI.setText(findTestObject('Locations/input_Location Nickname_nickname'), 'area15')

WebUI.click(findTestObject('Locations/button_Save'))

WebUI.delay(3)

WebUI.closeBrowser()

