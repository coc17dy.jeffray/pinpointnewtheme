<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_1</name>
   <tag></tag>
   <elementGuidId>66ca2f7d-170e-4692-a989-0583087709a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div/div/div/div/div/div[2]/div/div[3]/div/div/div/div/ol/li[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;App&quot;]/div[1]/div[1]/div[@class=&quot;app flex-row align-items-center animated fadeIn&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;justify-content-center row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card-group&quot;]/div[@class=&quot;text-white bg-primary  card&quot;]/div[@class=&quot;text-center card-body&quot;]/div[3]/div[1]/div[1]/div[@class=&quot;carouselWrapper&quot;]/div[@class=&quot;carousel slide&quot;]/ol[@class=&quot;carousel-indicators&quot;]/li[3]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div/div/div/div/div[2]/div/div[3]/div/div/div/div/ol/li[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]</value>
   </webElementXpaths>
</WebElementEntity>
