<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_TSP 1 _css-1hwfws3 select__value-contai_7172ea</name>
   <tag></tag>
   <elementGuidId>0d2067e7-d826-4283-ae6e-6680a00e9b5c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div/main/div/div/div/form/div/div/div[2]/div/div[3]/div/div[2]/div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-1hwfws3 select__value-container select__value-container--has-value</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;App&quot;]/div[1]/div[@class=&quot;app&quot;]/div[@class=&quot;app-body&quot;]/main[@class=&quot;main column-wrapper&quot;]/div[@class=&quot;main-container form-bk column-wrapper container-fluid&quot;]/div[@class=&quot;text-left p-5&quot;]/div[1]/form[1]/div[@class=&quot;dispatch-card&quot;]/div[@class=&quot;dispatch-card-sub demo-card card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;demo-card demo-card-content&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-8 col-md-9 col-lg-10&quot;]/div[@class=&quot;input-group&quot;]/div[@class=&quot;css-10nd86i basic-single select-in-form&quot;]/div[@class=&quot;css-2o5izw select__control select__control--is-focused select__control--menu-is-open&quot;]/div[@class=&quot;css-1hwfws3 select__value-container select__value-container--has-value&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/main/div/div/div/form/div/div/div[2]/div/div[3]/div/div[2]/div/div[2]/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TSP 1 *'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pinpoint Carrier 1'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TSP 1 is required.'])[1]/preceding::div[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/div[2]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
