<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ImportInstructionsBox</name>
   <tag></tag>
   <elementGuidId>d9298c51-6c65-4c40-b7db-541e3939191b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Import Instructions'])[7]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>EZCheck-In allows for importing certain data via a file in CSV format. To get started with importing carriers or brokers click the Download Import Template to obtain a sample CSV file. Add one row per carrier or broker that you wish to import.The following are the list of columns that should be included. Any fields with an asterisk are required and must be non blank. Any fields that contain a comma must be enclosed in quotes.Name*National Registration Number* Short Codes ContactsShort Codes should be entered as a comma separated string (if multiple short codes are provided then the field must be enclosed in quotes).Contacts should be entered as a comma separated string (if multiple contacts are provided then the field must be enclosed in quotes). For each contact provide the name, followed by a colon and then followed by the email address.  The following is an example:&quot;John Smith:jsmith@domain.com,Bill Smith:bsmith@domain.com&quot;Close</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;header-fixed sidebar-lg-show sidebar-fixed aside-menu-fixed aside-menu-off-canvas modal-open&quot;]/div[3]/div[1]/div[@class=&quot;modal fade show&quot;]/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Import Instructions'])[7]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='EZCheck-In allows for importing certain data via a file in CSV format. To get started with importing carriers or brokers click the Download Import Template to obtain a sample CSV file. Add one row per carrier or broker that you wish to import.']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
